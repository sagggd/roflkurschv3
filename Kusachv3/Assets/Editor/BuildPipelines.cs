using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

public enum TeamCityBuildType
{
    ios,
    android_il2cpp_apk,
    android_mono_apk,
    android_aab,
    windows_mono,
    windows_il2cpp
}

public class BuildConfig
{
    #region Args

    private class CommandLineArgs
    {
        public readonly string BuildNumber       = "0";
        public readonly string Version           = "1.0";
        public readonly string BundleVersionCode = "1";

        private const string BUILD_NUMBER        = "-build_number";
        private const string VERSION             = "-build_version";
        private const string BUNDEL_VERSION_CODE = "-bundle_version_code";

        public CommandLineArgs()
        {
            var args = Environment.GetCommandLineArgs();

            foreach (var arg in args)
            {
                Console.WriteLine($"Name: {arg}, Lenght: {arg.Length}");
            }

            for (var i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case BUILD_NUMBER:
                        BuildNumber = args[++i];
                        Debug.Log($"Find args: {nameof(BuildNumber)} with value: {BuildNumber}");
                        break;
                    case VERSION:
                        Version = args[++i];
                        Debug.Log($"Find args: {nameof(Version)} with value: {Version}");
                        break;
                    case BUNDEL_VERSION_CODE:
                        BundleVersionCode = args[++i];
                        Debug.Log($"Find args: {nameof(BundleVersionCode)} with value: {BundleVersionCode}");
                        break;
                }
            }
        }
    }

    #endregion

    public readonly TeamCityBuildType       TeamCityBuildType;
    public readonly BuildTarget             BuildTarget;
    public readonly BuildOptions            BuildOptions;
    public readonly BuildTargetGroup        BuildTargetGroup;
    public readonly ScriptingImplementation ScriptingBackend;

    public readonly string BundleVersionCode;
    public readonly string Version;
    public readonly string BuildNumber;

    public readonly string BuildPath;
    public readonly string KeystorePassword;
    public readonly string AliasPassword;

    public BuildConfig(TeamCityBuildType teamCityBuildType)
    {
        TeamCityBuildType = teamCityBuildType;

        BuildOptions = BuildOptions.None;

        BuildTarget = TeamCityBuildType switch
        {
            TeamCityBuildType.ios                 => BuildTarget.iOS,
            TeamCityBuildType.android_il2cpp_apk => BuildTarget.Android,
            TeamCityBuildType.android_mono_apk    => BuildTarget.Android,
            TeamCityBuildType.android_aab         => BuildTarget.Android,
            TeamCityBuildType.windows_mono        => BuildTarget.StandaloneWindows64,
            TeamCityBuildType.windows_il2cpp      => BuildTarget.StandaloneWindows64,

            _ => throw new BuildFailedException($"Invalid TeamCityBuildType: {TeamCityBuildType}")
        };

        ScriptingBackend = TeamCityBuildType switch
        {
            TeamCityBuildType.ios                 => ScriptingImplementation.IL2CPP,
            TeamCityBuildType.android_aab         => ScriptingImplementation.IL2CPP,
            TeamCityBuildType.android_il2cpp_apk => ScriptingImplementation.IL2CPP,
            TeamCityBuildType.windows_il2cpp      => ScriptingImplementation.IL2CPP,
            TeamCityBuildType.android_mono_apk    => ScriptingImplementation.Mono2x,
            TeamCityBuildType.windows_mono        => ScriptingImplementation.Mono2x,

            _ => throw new BuildFailedException(
                $"Not implemented ScriptingBackend for TeamCityBuildType: {TeamCityBuildType}"),
        };

        BuildTargetGroup = BuildTarget switch
        {
            BuildTarget.Android             => BuildTargetGroup.Android,
            BuildTarget.iOS                 => BuildTargetGroup.iOS,
            BuildTarget.StandaloneWindows64 => BuildTargetGroup.Standalone,

            _ => throw new BuildFailedException($"Not implemented build target group for target: {BuildTarget}"),
        };


        #region Keystore pass //TODO: Вынести в аргументы как пароль?

        var keystorePasswordPath = $"{Application.dataPath.Replace("Assets", "KeyStore")}/KeyPass.txt";

        if (File.Exists(keystorePasswordPath) == false)
        {
            Debug.LogError("KeyStore password files not found!");
            throw new BuildFailedException("key error");
        }

        var keyPass = File.ReadAllText(keystorePasswordPath);

        #endregion

        KeystorePassword = keyPass;
        AliasPassword    = keyPass;

        var arguments = new CommandLineArgs();

        BundleVersionCode = arguments.BundleVersionCode;
        Version           = arguments.Version;
        BuildNumber       = arguments.BuildNumber;

        #region BuildPath

        var applicationName = Application.productName;

        string CreatePath(string extension, params string[] folders)
        {
            var name = $"{Application.productName}_{BuildNumber}";
            name = Path.ChangeExtension(name, extension);

            var dataPath = $"{Application.dataPath.Replace("Assets", "builds")}";

            var path = Path.Combine(folders);
            path = Path.Combine(dataPath, path, name);
            return path;
        }

        BuildPath = TeamCityBuildType switch
        {
            TeamCityBuildType.ios                 => CreatePath(string.Empty, "ios"),
            TeamCityBuildType.android_il2cpp_apk => CreatePath(".apk", "android", "il2cpp"),
            TeamCityBuildType.android_mono_apk    => CreatePath(".apk", "android", "mono"),
            TeamCityBuildType.android_aab         => CreatePath(".aab", "android", "aab"),
            TeamCityBuildType.windows_mono        => CreatePath(".exe", "standalone", "mono", $"Build {BuildNumber}"),
            TeamCityBuildType.windows_il2cpp      => CreatePath(".exe", "standalone", "il2cpp", $"Build {BuildNumber}"),

            _ => throw new BuildFailedException($"Invalid TeamCityBuildType: {TeamCityBuildType}")
        };

        Debug.Log($"Build path: {BuildPath}");

        #endregion
    }
}

public class BuildPipelines : MonoBehaviour
{
    #region BuildTypes

    [MenuItem("Build/Build iOS")]
    public static void BuildiOS()
    {
        Debug.Log("============================ Build IOS ============================");

        Build(TeamCityBuildType.ios);

        Debug.Log("========================= Build IOS Done ===========================");
    }

    [MenuItem("Build/Build Android .apk (il2cpp)")]
    public static void BuildAndroidAPK_il2cpp()
    {
        Debug.Log("============================ Build Android apk IL2CPP ============================");

        Build(TeamCityBuildType.android_il2cpp_apk);

        Debug.Log("============================ Build Android apk IL2CPP Done ============================");
    }

    [MenuItem("Build/Build Android .apk (mono)")]
    public static void BuildAndroidAPK_mono()
    {
        Debug.Log("============================ Build Android apk mono ============================");

        Build(TeamCityBuildType.android_mono_apk);

        Debug.Log("============================ Build Android apk mono Done ============================");
    }

    [MenuItem("Build/Build Android .aab")]
    public static void BuildAndroidAAB()
    {
        Debug.Log("============================ Build Android aab ============================");

        Build(TeamCityBuildType.android_aab);

        Debug.Log("============================ Build Android aab Done ============================");
    }

    [MenuItem("Build/Build Windows (il2cpp)")]
    public static void BuildWindows_il2cpp()
    {
        Debug.Log("============================ Build Windows IL2CPP ============================");

        Build(TeamCityBuildType.windows_il2cpp);

        Debug.Log("============================ Build Windows IL2CPP Done ============================");
    }

    [MenuItem("Build/Build Windows (mono)")]
    public static void BuildWindows_mono()
    {
        Debug.Log("============================ Build Windows mono ============================");

        Build(TeamCityBuildType.windows_mono);

        Debug.Log("============================ Build Windows Done ============================");
    }

    #endregion

    private static void Build(TeamCityBuildType buildType)
    {
        if (string.IsNullOrEmpty(PlayerSettings.applicationIdentifier))
        {
            throw new BuildFailedException("Enter product name in Edit/Project Settings/Player/Product name");
        }

        var config = new BuildConfig(buildType);

        var scenePathInBuild = GetAllScenesPathInBuild();

        if (scenePathInBuild.Length == 0)
        {
            throw new BuildFailedException("Build setting has no scene");
        }

        SetUpPlayerSetting(config);

        var buildPlayerOptions = new BuildPlayerOptions
        {
            scenes           = scenePathInBuild,
            locationPathName = config.BuildPath,
            target           = config.BuildTarget,
            options          = config.BuildOptions
        };

        var report = BuildPipeline.BuildPlayer(buildPlayerOptions);

        var summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes");

            Debug.Log(config.BuildPath);
        }
        else if (summary.result == BuildResult.Failed)
        {
            Debug.LogError($"Build {config.TeamCityBuildType.ToString()} failed");
        }
    }

    private static string[] GetAllScenesPathInBuild()
    {
        #region Log

        void LogSceneInBuild(IReadOnlyList<string> pathScene)
        {
            var message = string.Empty;

            for (var i = 0; i < pathScene.Count(); i++)
            {
                var path = pathScene[i];
                message += $"path {i}: {path}{Environment.NewLine}";
            }

            Debug.Log(message);
        }

        #endregion

        var pathScenesInBuild = EditorBuildSettings.scenes
            .Where(scene => scene.enabled)
            .Select(scene => scene.path)
            .ToArray();

        LogSceneInBuild(pathScenesInBuild);

        return pathScenesInBuild;
    }

    private static void SetUpPlayerSetting(BuildConfig config)
    {
        PlayerSettings.bundleVersion = config.Version;

        PlayerSettings.SetScriptingBackend(config.BuildTargetGroup, config.ScriptingBackend);
        PlayerSettings.SetApiCompatibilityLevel(config.BuildTargetGroup, ApiCompatibilityLevel.NET_Standard_2_0);

        switch (config.BuildTarget)
        {
            case BuildTarget.Android:
                PlayerSettings.Android.bundleVersionCode   = Convert.ToInt32(config.BundleVersionCode);
                PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARM64 | AndroidArchitecture.ARMv7;
                EditorUserBuildSettings.buildAppBundle     = config.TeamCityBuildType == TeamCityBuildType.android_aab;
                PlayerSettings.keystorePass                = config.KeystorePassword;
                PlayerSettings.keyaliasPass                = config.AliasPassword;
                break;
            case BuildTarget.iOS:
                PlayerSettings.iOS.buildNumber = config.BundleVersionCode;
                break;
            case BuildTarget.StandaloneWindows64:
                //nothing
                break;
            default:
                throw new BuildFailedException($"Setup setting for target {config.BuildTarget} not implemented");
        }
    }
}