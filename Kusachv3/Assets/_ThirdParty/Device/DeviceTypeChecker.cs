﻿using UnityEngine;

namespace TS.Device
{
    public static class DeviceTypeChecker
    {
        public static bool IsTablet
        {
            get
            {
#if UNITY_IOS

                var deviceIsIpad = UnityEngine.iOS.Device.generation.ToString().Contains("iPad");

                return deviceIsIpad;

#elif UNITY_ANDROID

                float aspectRatio     = Mathf.Max(Screen.width, Screen.height) / Mathf.Min(Screen.width, Screen.height);
                var   isAndroidTablet = (DeviceDiagonalSizeInInches > 6.8f && aspectRatio < 2f);

                return isAndroidTablet;

#else
                return false;
#endif
            }
        }

        private static float DeviceDiagonalSizeInInches
        {
            get
            {
                float screenWidth    = Screen.width / Screen.dpi;
                float screenHeight   = Screen.height / Screen.dpi;
                float diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));
 
                return diagonalInches;
            }
        }
    }
}