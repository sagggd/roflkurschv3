﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace TS.ObjectPool
{
    public static class SharedGameObjectPool
    {
        private class GameObjectPool
        {
            private GameObject m_original;
        
            private Stack<GameObject> m_objs = new Stack<GameObject>();

            public GameObjectPool(GameObject original)
            {
                m_original = original;
            }

            public GameObject Rent() => m_objs.Count > 0 ? m_objs.Pop() : Object.Instantiate(m_original);

            public void Return(GameObject obj) => m_objs.Push(obj);
        }
        
        private class GameObjectEqualityComparer : IEqualityComparer<GameObject>
        {
            public bool Equals(GameObject x, GameObject y) => ReferenceEquals(x, y);
        
            public int GetHashCode(GameObject obj) => RuntimeHelpers.GetHashCode(obj);
        }
        
        
        
        private static readonly Dictionary<GameObject, GameObjectPool> m_pools =
            new Dictionary<GameObject, GameObjectPool>(new GameObjectEqualityComparer());

        public static GameObject Rent(GameObject original)
        {
            if (m_pools.TryGetValue(original, out var pool) == false)
                m_pools.Add(original, pool = new GameObjectPool(original));

            var obj = pool.Rent();
            obj.SetActive(true);
            
            return obj;
        }
        
        public static void Return(GameObject original, GameObject copy)
        {
            copy.SetActive(false);
            
            m_pools[original].Return(copy);
        }
    }
}