using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ButtonAnimation : MonoBehaviour
{
    [SerializeField] private float    _duration = 1f;
    [SerializeField] private Vector2  _endValue = new Vector2(2, 2);
    [SerializeField] private LoopType _loopType;
    [SerializeField] private Ease     _ease;

    private RectTransform _rectTransform;

    private Tween _tween;

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    private void Start()
    {
        _tween.Kill();
        _tween = _rectTransform.DOSizeDelta(_endValue, _duration).SetEase(_ease).SetLoops(-1, _loopType);
    }
}