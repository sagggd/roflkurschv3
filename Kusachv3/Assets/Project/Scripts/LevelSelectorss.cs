using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelectorss : MonoBehaviour
{
    [SerializeField] private Button _resetButton;
    [SerializeField] private Button _level1;
    [SerializeField] private Button _level2;
    [SerializeField] private Button _level3;

    private void Start()
    {
        _resetButton.onClick.AddListener(LevelManager.Instance.Reset);
        _level1.onClick.AddListener(() => outlevel(1));
        _level2.onClick.AddListener(() => outlevel(2));
        _level3.onClick.AddListener(() => outlevel(3));
    }
    private void outlevel(int i)
    {
        LevelManager.Instance.loadLevel(i);
    }
}
