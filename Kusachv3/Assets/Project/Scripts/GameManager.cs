using System;
using System.Collections;
using BurningKnight.PanelManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Project.Scripts
{
    public class GameManager : MonoBehaviour
    {
        #region Singleton

        private static GameManager _instance;

        public static GameManager Instance => 
            _instance == null ? _instance = FindObjectOfType<GameManager>() : _instance;

        #endregion
        
        private void Start()
        {
            GeneralPanelManager.Instance.OpenPanel<MainPanel>();
        }

        public void Exits()
        {
            Application.Quit();
        }
        
        public void StartGame()
        {
            GeneralPanelManager.Instance.OpenPanel<SelectorlvlPanel>();
        }
    }
}
