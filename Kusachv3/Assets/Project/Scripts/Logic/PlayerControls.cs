﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControls : MonoBehaviour
{
    public float Speed = 2;
    public GameObject Player;
    private Rigidbody2D componentRigidbody;
    public float bot;
    public float right;
    public float top;
    public float left;
    private void Start()
    {
        componentRigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        componentRigidbody.velocity = Vector2.zero;
        if (Input.GetKey(KeyCode.LeftArrow)) componentRigidbody.velocity += Vector2.left * Speed;
        if (Input.GetKey(KeyCode.RightArrow)) componentRigidbody.velocity += Vector2.right * Speed;
        if (Input.GetKey(KeyCode.UpArrow)) componentRigidbody.velocity += Vector2.up * Speed;
        if (Input.GetKey(KeyCode.DownArrow)) componentRigidbody.velocity += Vector2.down * Speed;
        if (Player.transform.localPosition.x < left) SceneManager.LoadSceneAsync("MainMenu");
        if (Player.transform.localPosition.y < bot) SceneManager.LoadSceneAsync("MainMenu");
        if (Player.transform.localPosition.x > right) SceneManager.LoadSceneAsync("MainMenu");
        if (Player.transform.localPosition.y > top) SceneManager.LoadSceneAsync("MainMenu");
    }
}