﻿using UnityEngine;

public class MazeSpawner : MonoBehaviour
{
    #region Singleton

    private static MazeSpawner _instance;

    public static MazeSpawner Instance => 
        _instance == null ? _instance = FindObjectOfType<MazeSpawner>() : _instance;

    #endregion
    public Cell CellPrefab;
    public Vector3 CellSize = new Vector3(1,1,0);
    public HintRenderer HintRenderer;
    
    public Maze maze;

    public int x;
    public int y;

    private void Start()
    {
        MazeGenerator generator = new MazeGenerator();
        maze = generator.GenerateMaze();

        for (int x = 0; x < maze.cells.GetLength(0); x++)
        {
            for (int y = 0; y < maze.cells.GetLength(1); y++)
            {
                Cell c = Instantiate(CellPrefab, new Vector3(x * CellSize.x, y * CellSize.y, y * CellSize.z), Quaternion.identity);
                c.WallLeft.SetActive(maze.cells[x, y].WallLeft);
                c.WallBottom.SetActive(maze.cells[x, y].WallBottom);
            }
        }

        HintRenderer.DrawPath();
    }
}