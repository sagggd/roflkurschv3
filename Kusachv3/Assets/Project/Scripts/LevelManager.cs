﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    #region Singleton

    private static LevelManager _instance;

    public static LevelManager Instance => 
        _instance == null ? _instance = FindObjectOfType<LevelManager>() : _instance;

    #endregion
    
    [SerializeField] private int levelUnLock;
    [SerializeField] private Button[] buttons;
    
    private void Start()
    {
        levelUnLock = PlayerPrefs.GetInt("levels", 1);

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = false;
        }

        for (int i = 0; i <levelUnLock; i++)
        {
            buttons[i].interactable = true;
        }
    }
    public void loadLevel(int level)
    {
        SceneManager.LoadScene(level);
    }
    public void Reset()
    {
        for (int i = 1; i < buttons.Length; i++)
        {
            buttons[i].interactable = false;
        }
        PlayerPrefs.DeleteAll();
    }
    
}
