using BurningKnight.PanelManager;
using Project.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectorlvlPanel : PanelBase
{
    [SerializeField] private Button _level1;
    [SerializeField] private Button _level2;
    [SerializeField] private Button _level3;
    
    public override void Init()
    {
    }

    protected override void OnOpened()
    {
        _level1.onClick.AddListener(() => SceneManager.LoadSceneAsync("Easy"));
        _level2.onClick.AddListener(() => SceneManager.LoadSceneAsync("Medium"));
        _level3.onClick.AddListener(() => SceneManager.LoadSceneAsync("Hard"));
    }

    protected override void OnClosed()
    {
        _level1.onClick.RemoveListener(() => SceneManager.LoadSceneAsync("Easy"));
        _level2.onClick.RemoveListener(() => SceneManager.LoadSceneAsync("Medium"));
        _level3.onClick.RemoveListener(() => SceneManager.LoadSceneAsync("Hard"));
    }
}