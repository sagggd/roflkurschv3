using System;
using System.Threading;
using BurningKnight.PanelManager;
using Project.Scripts;
using TMPro;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MainPanel : PanelBase
{
    [SerializeField] private Button          _startButton;
    [SerializeField] private Button          _exitButton;

    private UnityAction startGame;
    private UnityAction exitGame;
    

    public override void Init()
    {
        startGame = GameManager.Instance.StartGame;
        exitGame = GameManager.Instance.Exits;
    }

    protected override void OnOpened()
    {
        _startButton.onClick.AddListener(startGame);
        _exitButton.onClick.AddListener(exitGame);
    }

    protected override void OnClosed()
    {
        _startButton.onClick.RemoveListener(startGame);
        _exitButton.onClick.RemoveListener(exitGame);
    }
}