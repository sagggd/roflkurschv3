using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walk : MonoBehaviour
{
    [SerializeField] Rigidbody2D _rb;
    [SerializeField] private float _x;
    
    private void Update()
    {
        _x = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate()
    {
        _rb.AddForce(new Vector2(_x * 150,0f));
    }
}
